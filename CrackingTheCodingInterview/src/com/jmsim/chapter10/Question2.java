package com.jmsim.chapter10;

import java.util.Arrays;
import java.util.Comparator;

public class Question2 {
	
	public static int primes[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101};
	
	public static long stringToNum(String item){
		long val = 1;
		
		item = item.toLowerCase();
		for(int i = 0 ; i < item.length() ; i++){
			char c = item.charAt(i);
			val *= Question2.primes[ ((int)c) - ((int)'a') ];
		}
		return val;
	}
	
	public static void groupAnagrams(String[] arr){
		for(int i = 0 ; i < arr.length ; i++){
			System.out.println(arr[i] + " >> "+ stringToNum(arr[i]));
		}
		Comparator<String> comp = new Comparator<String>(){
			@Override
			public int compare(String a, String b){
				if(a.length() != b.length()){
					return a.length() - b.length();
				}
				
				return Long.compare(Question2.stringToNum(a), (Question2.stringToNum(b)));
				
			}
		};
		Arrays.sort(arr, comp);
		for(int i = 0;  i < arr.length ; i++){
			System.out.println(arr[i]);
		}
		return;
	}
	
	
	
	public static void main(String[] args){
		System.out.println(Question2.primes.length);
		
		String arr[] = new String[10];
		arr[0] = "Abc";
		arr[1] = "c";
		arr[2] = "zef";
		arr[3] = "qwergdgv";
		arr[4] = "ddfxzcv";
		arr[5] = "cAb";
		arr[6] = "acc";
		arr[7] = "y";
		arr[8] = "jasd";
		arr[9] = "acb";
		
		Question2.groupAnagrams(arr);
		
//		System.out.println((int)'a');
	}
	
	
}
