package com.jmsim.chapter10;

public class Question1 {
	
	public static void pushArrayTo(Integer[] arr, int startIdx, int endIdx){
		int len = 0;
		for(int i = startIdx ; i < arr.length ; i++){
			if(arr[i] == null){
				break;
			}
			len++;
		}
		
		Integer[] tmp = new Integer[len];
		for(int i = 0 ; i < len ; i++){
			tmp[i] = arr[i+startIdx];
			arr[i+startIdx]=null;
		}
		for(int i = 0 ; i < len ; i++){
			arr[i+endIdx] = tmp[i];
		}
		
	}
	
	public static void sortedMerge(Integer[] a, Integer[] b){
		for(int i = 0 ; i < b.length ; i++){
//			System.out.println("i : "+i);
			boolean inserted = false;
			for(int j = 0 ; j < a.length ; j++){
//				System.out.println(">> j : "+j);
				if(a[j] == null){
					a[j] = b[i];
					inserted = true;
					break;
				}else if(a[j]>b[i]){
					pushArrayTo(a, j, j+1);
					a[j] = b[i];
					inserted = true;
					break;
				}else{
					continue;
				}
				
			}
			if(inserted){
//				for(int iii = 0 ; iii < a.length ; iii++){
//					System.out.println(a[iii]);
//				}
				continue;
			}
			
		}
		return;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Integer[] a = new Integer[10];
		Integer[] b = new Integer[5];
		
		a[0] = 1;
		a[1] = 2;
		a[2] = 3;
		a[3] = 4;
		a[4] = 5;
		
		b[0] = 2;
		b[1] = 3;
		b[2] = 7;
		b[3] = 8;
		b[4] = 9;
				
//		Question1.pushArrayTo(a,3, 8);
		Question1.sortedMerge(a, b);
		
		
		for(int i = 0 ; i < a.length ; i++){
			System.out.println(a[i]);
		}
	}

}
