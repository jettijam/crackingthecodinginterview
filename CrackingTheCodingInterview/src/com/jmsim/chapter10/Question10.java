package com.jmsim.chapter10;

import java.util.Arrays;

public class Question10 {
	private MinHeap heap = new MinHeap();
	public void track(int i) {
		heap.add(i);
	}

	public int getRankOfNumber(int i) {
		return heap.rank(i);
	}

	public static void main(String[] args) {
		Question10 test = new Question10();
		test.track(5);
		test.track(1);
		test.track(4);
		test.track(4);
		test.track(5);
		test.track(9);
		test.track(7);
		test.track(13);
		test.track(3);

		//		System.out.println(test.getRankOfNumber(1));
		//		System.out.println(test.getRankOfNumber(3));
		System.out.println(test.getRankOfNumber(4));
	}
}

class MinHeap{
	private int capacity = 100;
	private Integer[] items = new Integer[capacity];
	private int size = 0;
	public MinHeap() {

	}

	private void buildMinHeap() {
		for (int i = (size >>> 1) - 1; i >= 0; i--) {
			heapifyDown(i);
		}
	}
	private int getLeftChildIndex(int parentIndex) { return (parentIndex << 1) + 1; }
	private int getRightChildIndex(int parentIndex) { return (parentIndex << 1) + 2; }
	private int getParentIndex(int childIndex) { return (childIndex - 1) >> 1; }

	private boolean hasLeftChild(int index) { return getLeftChildIndex(index) < size; }
	private boolean hasRightChild(int index) { return getRightChildIndex(index) < size; }
	private boolean hasParent(int index) { return getParentIndex(index) >= 0; }

	private int leftChild(int index) { return items[getLeftChildIndex(index)]; }
	private int rightChild(int index) { return items[getRightChildIndex(index)]; }
	private int parent(int index) { return items[getParentIndex(index)]; }

	private void swap(int indexA, int indexB) {
		int temp = items[indexA];
		items[indexA] = items[indexB];
		items[indexB] = temp;
	}

	private void ensureExtraCapacity() {
		if (size == capacity) {
			items = Arrays.copyOf(items, capacity << 1);
			capacity = capacity << 1;
		}
	}

	private static boolean less(Comparable v, Comparable w){ return v.compareTo(w) < 0; }
	private void heapifyUp(int index) {                                     
		while (hasParent(index) && less(items[index], parent(index))) {      
			int parentIndex = getParentIndex(index);                        
			swap(index, parentIndex);                                       
			index = parentIndex;                                            
		}
	}
	private void heapifyDown(int index) {                                               
		while (hasLeftChild(index)) {                                                                    
			int smallerChildIndex = getLeftChildIndex(index);                           
			if ( hasRightChild(index) && less( rightChild(index), leftChild(index) ) )  
				smallerChildIndex = getRightChildIndex(index);  

			if (less(items[index], items[smallerChildIndex]))                           
				break;                                                                                          
			else                                                                        
				swap(index, smallerChildIndex);                

			index = smallerChildIndex;                                                  
		}
	}
	public int peek() {
		if (size == 0) throw new IllegalStateException();
		return items[0];
	}
	public int poll() {
		if (size == 0) throw new IllegalStateException();   
		int item = items[0];                                  
		items[0] = items[--size];                           
		items[size] = null;                                 
		heapifyDown(0);                                     
		return item;
	}
	public void add(Integer item) {
		ensureExtraCapacity();      
		items[size++] = item;       
		heapifyUp(size - 1);        
	}

	public Integer[] heapSort() {
		Integer[] copyItems = Arrays.copyOf(items, items.length);
		for (int i=size - 1; i >= 0; i--) {
			Integer max = poll();

			items[i] = max;
		}
		Integer[] result = items;
		items = copyItems;
		return result;
	}

	public int rank(int num ) {
		int val = 0;
		int sameNumber = 0;
		Integer[] copyItems = Arrays.copyOf(items, items.length);
		for (int i=size - 1; i >= 0; i--) {
			Integer max = poll();
			System.out.println(">>"+max);

			if(max > num) {
				return val + sameNumber - 1;
			}

			if(max == num) {
				sameNumber++;
			}else {

				val++;
			}
			items[i] = max;
		}

		items = copyItems;
		return val;
	}
}