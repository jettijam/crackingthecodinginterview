package com.jmsim.chapter10;

public class Question3 {
	public static int searchInRotatedArray(int num, int[] arr){
		for(int i = 0 ; i < arr.length ; i++){
			if(arr[i] == num){
				return i;
			}
		}
		return -1;
	}
	
	public static void main(String[] args){
		
	}
}
