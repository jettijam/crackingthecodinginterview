package com.jmsim.chapter10;

public class Question5 {
	public static int sparseSearch(String[] arr, String val){
		return recurSparseSearch(arr, 0, arr.length-1, val);
	}
	
	public static int recurSparseSearch(String[] arr, int start, int end, String val){
		String startVal = arr[start];
		String endVal = arr[end];
		
		while(startVal.equals("") && start < end){
			start++;
			startVal = arr[start];
		}
		
		while(endVal.equals("") && end > start){
			end--;
			endVal = arr[end];
		}
		
		int mid = (end - start)/2;
		String midVal = arr[mid];
		
		if(startVal.equals(val)){
			return start;
		}
		int midResult = val.compareTo(midVal);
		if(midResult < 0){
			return recurSparseSearch(arr, start+1, mid-1, val);
		}
		if(midResult == 0){
			return mid;
		}
		
		int endResult = val.compareTo(endVal);
		if(endResult < 0){
			return recurSparseSearch(arr, mid+1, end-1, val);
		}
		if(endResult == 0){
			return end;
		}
		return -1;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] arr = {"at", "", "", "", "ball", "", "", "car", "", "", "dad", "", ""};
		System.out.println(Question5.sparseSearch(arr, "car"));
	}

}
