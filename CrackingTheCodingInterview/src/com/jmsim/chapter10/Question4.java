package com.jmsim.chapter10;

import java.util.ArrayList;

public class Question4 {
	
	public int sortedSearch(Listy input, int val){
		int prevIdx = 0, curIdx = 0;
		int prevVal = 0, curVal = 0;
		
		curIdx = 0;
		curVal = input.elementAt(curIdx);
		if(curVal == -1){
			return -1;
		}
		if(curVal == val){
			return curIdx;
		}
		prevIdx = curIdx;
		prevVal = curVal;
		
		curIdx = 1;
		curVal = input.elementAt(curIdx);
		if(curVal == -1){
			return -1;
		}
		if(curVal == val){
			return curIdx;
		}
		
		
		return startEndSortedSearch(input, 0, 1, val);
		
//		return -1;
	}
	
	
	public int startEndSortedSearch(Listy input, int start, int end, int val){
		if(end < start){
			return -1;
		}
		int mid = (start + end)/2;
		int len = (end - start + 1);
		int startVal = input.elementAt(start);
		int endVal = input.elementAt(end);
		int midVal = input.elementAt(mid);
		System.out.println(String.format("%d ~ %d -> %d ~ %d", start, end, startVal, endVal));

		if(startVal == -1){
			return -1;
		}
		if(midVal == -1){
			return startEndSortedSearch(input, start+1, mid-1, val);
		}
		if(endVal == -1){
			return startEndSortedSearch(input, mid+1, end-1, val);
		}
		if(val == startVal){
			return start;
		}
		if(val == midVal){
			return mid;
		}
		if(val == endVal){
			return end;
		}
		if(len == 0){
			return -1;
		}
		
		if(val < midVal){
			return startEndSortedSearch(input, start+1, mid-1, val);
		}
		if(val < endVal){
			return startEndSortedSearch(input, mid+1, end-1, val);
		}
		
		return startEndSortedSearch(input, end, end*2, val);
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Listy listy = new Listy();
		listy.arr.add(1);
		listy.arr.add(2);
		listy.arr.add(3);
		listy.arr.add(4);
		listy.arr.add(5);
		listy.arr.add(5);
		listy.arr.add(5);
		listy.arr.add(5);
		
		Question4 test = new Question4();
		System.out.println(test.sortedSearch(listy, 4));
	}

}
class Listy{
	ArrayList<Integer> arr = new ArrayList<Integer>();
	public int elementAt(int i){
		if(i < arr.size())
			return arr.get(i);
		return -1;
	}
}