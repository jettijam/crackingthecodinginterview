package com.jmsim.chapter10;

import java.util.Arrays;

public class Question11 {
	public static int[] peakAndValleys(int[] arr) {
		Arrays.sort(arr);
//		int[] res = new int[arr.length];
		
//		Arrays.sort(res);
		for(int i = 1 ; i < arr.length ; i+=2) {
			swap(arr, i-1, i);
		}
			
		
		return arr;
	}
	public static void swap(int[] arr, int a, int b) {
		int tmp = arr[a];
		arr[a] = arr[b];
		arr[b] = tmp;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = {0, 1, 4, 7, 8, 9};
		
		
		arr = Question11.peakAndValleys(arr);
		for(int i = 0 ; i < arr.length ; i++) {
			System.out.println(arr[i]);
		}
	}

}
