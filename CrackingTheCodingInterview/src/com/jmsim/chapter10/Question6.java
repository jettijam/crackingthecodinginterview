package com.jmsim.chapter10;

public class Question6 {
	public static void main(String[] args){
		System.out.println("20기가 파일은 메모리에 올려놓고 작업하기 너무 큼. -> 처음 큰 파일을 돌면서 알파벳 첫글자를 기준으로 파일을 나눈다 -> 26개의 파일 (알파벳 갯수 : 26)");
		System.out.println("알파벳 순으로 정렬될 경우 무조건 b파일에 속한 문장들은 a파일보다 뒤에 있을 수 밖에 없음.");
		System.out.println("만약 특정 알파벳에 해당하는 파일이 너무 클 경우(> 2GB 정도 .. ), 알파벳의 두번째 글자를 기준으로 파일을 다시 나눈다 (i_a.txt, i_b.txt, i_c.txt ...)");
		System.out.println("각 파일을 돌면서 sortin한 뒤 모든 파일을 순서대로 concatenate하 됨.");
		System.out.println("   한 파일 내부에서도 위와 같은 방식(첫 글자부터 알파벳에 맞춰)으로 소팅할 수 있");
	}
}
