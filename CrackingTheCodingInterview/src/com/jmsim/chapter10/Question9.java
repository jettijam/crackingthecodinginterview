package com.jmsim.chapter10;

public class Question9 {
	public static int sortedMatrixSearch(int[][] mat, int val) {
		for(int y = 0 ; y < mat.length ; y++) {
			int[] row = mat[y];
//			for(int i = 0 ; i < row.length ; i++) {
//				System.out.print(row[i]);
//				System.out.print(" ");
//			}
//			System.out.println();
//			if(row[0] == val) {
//				System.out.println(String.format("(x,y) : (%d, %d)", 0, y));
//				return 1;
//			}
			if(row[0] <= val) {
				int res = binarySearch(row, val);
				if(res > -1) {
					System.out.println(String.format("(x,y) : (%d, %d)", res, y));
					return 1;
				}
			}
		}
		return -1;
	}
	
	public static int binarySearch(int[] arr, int val) {
		
		return recurBinarySearch(arr, 0, arr.length-1, val);
	}
	
	public static int recurBinarySearch(int[] arr, int start, int end, int val) {
		
		int startVal = arr[start];
		int endVal = arr[end];
		int mid = (start+end)/2;
		int midVal = arr[mid];
		
		if(val == startVal) {
			return start;
		}
		if(val == midVal) {
			return mid;
		}
		if(val == endVal) {
			return end;
		}
		if(start == end) {
			return -1;
		}
		if(val < midVal) {
			return recurBinarySearch(arr, start+1, mid-1, val);
		}
		if(val < endVal) {
			return recurBinarySearch(arr, mid+1, end-1, val);
		}
		
		return -1;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//1 3  5  7
		//2 6  10 14
		//4 9  15 21
		//8 12 20 28
		int[][] mat = new int[4][4];
		mat[0][0] = 1;
		mat[0][1] = 3;
		mat[0][2] = 5;
		mat[0][3] = 7;
		
		mat[1][0] = 2;
		mat[1][1] = 6;
		mat[1][2] = 10;
		mat[1][3] = 14;
		
		mat[2][0] = 4;
		mat[2][1] = 9;
		mat[2][2] = 15;
		mat[2][3] = 21;
		
		mat[3][0] = 8;
		mat[3][1] = 12;
		mat[3][2] = 20;
		mat[3][3] = 28;
		
		Question9.sortedMatrixSearch(mat, 6);

	}

}
