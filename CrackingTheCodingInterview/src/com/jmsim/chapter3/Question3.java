package com.jmsim.chapter3;

import java.util.ArrayList;

import com.jmsim.chapter3.EmptyStackException;

public class Question3 {
	
	class SetOfStacks{
		ArrayList<MyStack> stacks = new ArrayList<MyStack>(); // array로 관리하기 귀춘 ..
		ArrayList<Integer> sizes = new ArrayList<Integer>(); // stack class내부에 size관련 함수를 넣지 않기로 결정..!
		int curStackIdx = -1;
		int THRES = 10;
		public SetOfStacks(){
//			stacks.add(new MyStack());
//			sizes.add(0);
		}
		
		public int pop() throws EmptyStackException{
			if(curStackIdx < 0){
				throw new EmptyStackException();
			}
			
			int result = stacks.get(curStackIdx).pop();
			sizes.set(curStackIdx, sizes.get(curStackIdx)-1);

			
			if(sizes.get(curStackIdx) <= 0){
				sizes.remove(curStackIdx);
				stacks.remove(curStackIdx);
				curStackIdx --;
			}
			return result;
		}
		
		public void push(int item){
//			if(curStackIdx >= 0)
//				System.out.println("STACK #"+curStackIdx+" SIZE: "+sizes.get(curStackIdx));
			if(curStackIdx < 0 || sizes.get(curStackIdx) >= THRES){
				stacks.add(new MyStack());
				sizes.add(0);
				curStackIdx ++;
			}
			
			stacks.get(curStackIdx).push(item);
			sizes.set(curStackIdx, sizes.get(curStackIdx)+1);
		}
		
		public int popAt(int stackIdx) throws EmptyStackException{
			if(stackIdx > stacks.size() || stackIdx < 0){
				throw new EmptyStackException();
			}
			
			int result = stacks.get(stackIdx).pop();
			sizes.set(stackIdx, sizes.get(stackIdx)-1);
			if(sizes.get(stackIdx) <= 0){
				stacks.remove(stackIdx);
				sizes.remove(stackIdx);
				curStackIdx --;
			}
			return result;
			
		}
		
		public void printStacks(){
			System.out.println("=================================");
			for(int i = 0 ; i < curStackIdx+1 ; i++){
				System.out.println("STACK "+i);
				stacks.get(i).printStack();
				System.out.println();
			}
		}
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Question3 test = new Question3();
		SetOfStacks stack = test.new SetOfStacks();
		
		for(int i = 0 ; i < 30 ; i ++){
			stack.push(i);
		}
		
		for(int i = 0 ; i < 11 ; i++){
			try {
				System.out.println(stack.popAt(1));
			} catch (EmptyStackException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		stack.printStacks();
	}

}
