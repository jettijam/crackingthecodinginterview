package com.jmsim.chapter3;

public class Question1 {
	
	public class ArrayStack{
		int arr[] = new int[100];
		int base[] = new int[3];
		int top[] = new int[3];
		
		public ArrayStack(){
			base[0] = 0;
			base[1] = 10;
			base[2] = 20;
			top[0] = 0;
			top[1] = 10;
			top[2] = 20;
		}
		
		public void push(int stackNum, int item){
			if(stackNum == 2){
				if(top[stackNum]+1 >= arr.length-1){
					biggerArray();
				}
			}else{
				if(top[stackNum]+1 >= base[stackNum+1]){
					shiftStacks(stackNum+1);
				}
			}
			arr[top[stackNum]] = item;
			top[stackNum]++;
		}
		
		private void biggerArray(){
			System.out.println("biggerArray called");

			int new_arr[] = new int[arr.length + 100];
			for(int i = 0 ; i < arr.length ; i++){
				new_arr[i] = arr[i];
			}
			arr = new_arr;
		}
		
		private void shiftStacks(int stackNum){
//			System.out.println("shiftStack called");
			int curStackNum = 2;
			int step = 10;	
			while(curStackNum >= stackNum){
				int curStep = step * (curStackNum+1);
				
				int newBase = base[curStackNum] + curStep;
				int stackSize = top[curStackNum] - base[curStackNum];
				int newTop = newBase + stackSize;
				if(newTop >= arr.length-1){
					biggerArray();
				}
				
				int newArr[] = new int[stackSize];
				for(int i = 0 ; i < stackSize ; i++){
					newArr[i] = arr[base[curStackNum]+i];
				}
				for(int i = 0 ; i < stackSize ; i++){
					arr[newBase+i] = newArr[i];
				}
				
				curStackNum--;
			}
		}
		
		public int pop(int stackNum) throws EmptyStackException{
			if(isEmpty(stackNum)){
				throw new EmptyStackException();
			}
			int item = arr[--top[stackNum]];
			return item;
		}
		
		public int peek(int stackNum) throws EmptyStackException{
			if(isEmpty(stackNum))
				throw new EmptyStackException();
			return arr[top[stackNum]-1];
		}
		
		public boolean isEmpty(int stackNum){
			return base[stackNum] == top[stackNum];
		}
		
		public void printStacks(){
			for(int stackNum = 0 ; stackNum < 3 ; stackNum++){
				
				int stackSize = top[stackNum] - base[stackNum];
				System.out.println("========================================");
				System.out.println("STACK "+stackNum+"  (length : "+stackSize+")");
				for(int i = 0 ; i < stackSize ; i++){
					System.out.println("    "+arr[base[stackNum]+i]);
				}
				System.out.println("========================================");

			}
			System.out.println();

		}
	}
	class EmptyStackException extends Exception{
		
	}
	
	public static void main(String[] args){
		Question1 test = new Question1();
		ArrayStack stack = test.new ArrayStack();
		for(int i=0;i<100;i++)
			stack.push(0, 8);
		stack.push(2, 4);
//		stack.printStacks();
//		try {
//			for(int i=0;i<50;i++)
//				System.out.println(stack.pop(0));
//		} catch (EmptyStackException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		stack.printStacks();
	}
}
