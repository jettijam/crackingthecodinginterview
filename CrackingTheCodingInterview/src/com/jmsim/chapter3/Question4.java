package com.jmsim.chapter3;

public class Question4 {
	class MyQueue{
		MyStack stackQ = new MyStack();
		MyStack tmpStack = new MyStack();
		
		public void enqueue(int item) throws EmptyStackException{
			while(!stackQ.isEmpty()){
				tmpStack.push(stackQ.pop());
			}
			tmpStack.push(item);
			
			while(!tmpStack.isEmpty()){
				stackQ.push(tmpStack.pop());
			}
		}
		
		public int dequeue() throws EmptyStackException{
			if(stackQ.isEmpty())
				throw new EmptyStackException();
			return stackQ.pop();
		}
	}
	
	public static void main(String[] args){
		Question4 test = new Question4();
		MyQueue queue = test.new MyQueue();
		try {
			queue.enqueue(1);
			queue.enqueue(2);
			queue.enqueue(3);
			System.out.println(queue.dequeue());
			System.out.println(queue.dequeue());
			System.out.println(queue.dequeue());
		} catch (EmptyStackException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
