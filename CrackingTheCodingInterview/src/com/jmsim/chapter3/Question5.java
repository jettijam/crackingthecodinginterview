package com.jmsim.chapter3;

public class Question5 {
	
	
	public void log(int part, MyStack origin, MyStack newStack, Integer hand) throws EmptyStackException{
		int origin_peek = origin.isEmpty()?-9999:origin.peek();
		int new_peek = newStack.isEmpty()?-9999:newStack.peek();
		System.out.print("PART "+part+" : ");
		System.out.println(origin_peek +" " + hand + " " +new_peek);
	}
	
	public Integer sortPart2(MyStack origin, MyStack newStack, Integer hand) throws EmptyStackException{
		
		if(origin.isEmpty()){
			log(0, origin, newStack, hand);
			return sortPart3(origin, newStack, hand);
		}
		
		
		if(hand > origin.peek()){
			log(1, origin, newStack, hand);
			return sortPart3(origin, newStack, hand);
		}else{
			log(2, origin, newStack, hand);
			Integer tmp = hand;
			hand = origin.pop();
			origin.push(tmp);
			return sortPart3(origin, newStack, hand);
		}
	}
	
	public Integer sortPart3(MyStack origin, MyStack newStack, Integer hand) throws EmptyStackException{
		if(newStack.isEmpty()){
			log(4, origin, newStack, hand);
//			System.out.println("NOW HAND IS EMPTY");
			newStack.push(hand);
			hand = null;
			return hand;
		}
		if(hand > newStack.peek()){
			log(5, origin, newStack, hand);
			origin.push(newStack.pop());
			hand = sortPart3(origin, newStack, hand);
			if(hand == null){
				return hand;
			}
			
			return sortPart2(origin, newStack, hand);
		}else{
			log(6, origin, newStack, hand);
			newStack.push(hand);
			return null;
		}
	}
	
	
	public MyStack sortStack(MyStack origin) throws EmptyStackException{
		MyStack newStack = new MyStack();
		Integer hand = null;
		
		while(!origin.isEmpty()){
			if(hand == null){
				hand = origin.pop();
			}
			hand = sortPart2(origin, newStack, hand);
			
		}
		
		return newStack;
	}
	
	
	
	public static void main(String[] args){
		MyStack stack = new MyStack();
		stack.push(5);
		stack.push(4);
		stack.push(3);
		stack.push(2);
		stack.push(1);
		
		stack.printStack();
		System.out.println("**********************");
		Question5 test = new Question5();
		try {
			stack = test.sortStack(stack);
		} catch (EmptyStackException e) {
			e.printStackTrace();
		}
		
		System.out.println("**********************");
		stack.printStack();
	
	}
}
