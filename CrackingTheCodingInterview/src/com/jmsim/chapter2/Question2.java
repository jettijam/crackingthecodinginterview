package com.jmsim.chapter2;

public class Question2 {
	public LinkedListNode kthElementToLast(LinkedListNode list, int k){ //잘못 이해함 .. ;;
		LinkedListNode result = list;
		for(int i = 0 ; i < k ; i++){
			result.head = result.head.next;
		}
		return result;
	}
	
	public int kthElementToLast2(LinkedListNode list, int k){
		LinkedListNode.Node cursor = list.head;
		int length = 0;
		while(cursor!=null){
			length ++;
			cursor = cursor.next;
		}
		cursor = list.head;
		for(int i = 0 ; i < length-k-1 ; i++){
			cursor = cursor.next;
		}
		
		return cursor.data;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedListNode list = new LinkedListNode();
		for(int i = 0 ; i < 20 ; i ++){
			list.appendToTail(i);
		}
		
//		list.printList();
//		System.out.println();
		
		Question2 test = new Question2();
		System.out.println(test.kthElementToLast2(list, 20));
	}

}
