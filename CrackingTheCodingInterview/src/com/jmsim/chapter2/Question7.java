package com.jmsim.chapter2;

import java.util.HashSet;
import java.util.Set;

public class Question7 {
	public boolean intersection(LinkedListNode list1, LinkedListNode list2){
		Set<LinkedListNode.Node> set = new HashSet<LinkedListNode.Node>();
		LinkedListNode.Node p = list1.head;
		while(p!=null){
			set.add(p);
			p = p.next;
		}
		
		p = list2.head;
		while(p!=null){
			if(set.contains(p))
				return true;
			p = p.next;
		}
		return false;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedListNode.Node node = new LinkedListNode().new Node(5);
		
		LinkedListNode list1 = new LinkedListNode();
		LinkedListNode list2 = new LinkedListNode();
		
		list1.appendToTail(7);
		list1.appendToTail(1);
		list1.appendToTail(6);
		
		LinkedListNode.Node p = list1.head;
		while(p.next != null)
			p = p.next;
		p.next = node;
		
		list2.appendToTail(5);
		list2.appendToTail(9);
		list2.appendToTail(1);
		list2.appendToTail(2);
		list2.appendToTail(2);
		
		p = list2.head;
		while(p.next != null)
			p = p.next;
//		p.next = node;
		
		Question7 test = new Question7();
		System.out.println(test.intersection(list1, list2));
	}

}
