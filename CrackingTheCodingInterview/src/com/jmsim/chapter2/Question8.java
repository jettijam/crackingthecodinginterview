package com.jmsim.chapter2;

public class Question8 {
	public LinkedListNode.Node loopDetection(LinkedListNode list){
		LinkedListNode.Node turtle = list.head;
		LinkedListNode.Node hare = turtle.next.next;
		while(true){
			if(turtle == hare)
				return turtle;
			turtle = turtle.next;
			hare = hare.next.next;
		}
	}
	
	public static void main(String[] args){
		LinkedListNode list1 = new LinkedListNode();
		for(int i = 0 ; i < 20 ; i++){
			list1.appendToTail(i);
		}
		
		
		
		LinkedListNode.Node loop = list1.head.next.next.next.next.next.next.next.next.next;
		
		LinkedListNode.Node p = list1.head;
		while(p.next != null)
			p = p.next;
		p.next = loop;
		
		list1.printList();
		
		Question8 test = new Question8();
		System.out.println(test.loopDetection(list1).data);
	}
}
