package com.jmsim.chapter2;


public class Question1 {
	public void removeDup(LinkedListNode list){
		LinkedListNode.Node p1 = list.head;
		
		while(p1 != null){
			LinkedListNode.Node p2 = p1;
			while(p2.next!=null){
				if(p2.next.data == p1.data){
					p2.next = p2.next.next;
				}else{
					p2 = p2.next;
				}
			}
			p1 = p1.next;
			
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		LinkedListNode list = new LinkedListNode();
		list.appendToTail(1);
		list.appendToTail(2);
		list.appendToTail(2);
		list.appendToTail(2);
		list.appendToTail(3);
		list.appendToTail(3);
		list.appendToTail(3);
		
		list.printList();
		System.out.println();
		Question1 test = new Question1();
		test.removeDup(list);
		list.printList();
//		ListIterator<Integer> itr = list.listIterator();
//		while(itr.hasNext()){
//			Integer val = itr.next();
//			System.out.println(val);
//		}
	}

}
