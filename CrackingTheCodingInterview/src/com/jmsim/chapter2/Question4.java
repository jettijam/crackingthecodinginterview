package com.jmsim.chapter2;

public class Question4 {
	public LinkedListNode partition(LinkedListNode list, int partition){
		LinkedListNode result = new LinkedListNode();
		LinkedListNode result_big = new LinkedListNode();
		LinkedListNode.Node p = list.head;
		
		while(p!= null){
			if(p.data < partition){
				System.out.println("small! " +p.data);
				result.appendToTail(p.data);
			}else{
				result_big.appendToTail(p.data);
			}
			p = p.next;
		}
		
		p = result.head;
		while(p.next != null){
			p = p.next;
		}
		p.next = result_big.head;
		
		
		return result;
	}
	public static void main(String[] args) {
		LinkedListNode list = new LinkedListNode();
		list.appendToTail(4);
		list.appendToTail(5);
		list.appendToTail(3);
		list.appendToTail(2);
		list.appendToTail(6);
		list.appendToTail(1);
		list.appendToTail(4);
		
		list.printList();
		System.out.println();
		
		Question4 test = new Question4();
		test.partition(list, 4).printList();
	}

}
