package com.jmsim.chapter2;

public class Question5 {
	public LinkedListNode sumLists(LinkedListNode list1, LinkedListNode list2){
		LinkedListNode result = new LinkedListNode();
		
		LinkedListNode.Node p1 = list1.head;
		LinkedListNode.Node p2 = list2.head;
		
		while(p1!=null){
			
			int val = p1.data + p2.data;
			if(val > 10){
				if(p1.next != null)
					p1.next.data ++;
				else if(p2.next != null)
					p2.next.data ++;
				else
					list1.appendToTail(1);
				val -= 10;
			}
			result.appendToTail(val);
			
			p1 = p1.next;
			p2 = p2.next;
			if(p1 == null){
				if(p2 != null){
					LinkedListNode.Node tmp = result.head;
					while(tmp.next != null){
						tmp = tmp.next;
					}
					tmp.next = p2;
					break;
				}
			}else{
				if(p2 == null){
					LinkedListNode.Node tmp = result.head;
					while(tmp.next != null){
						tmp = tmp.next;
					}
					tmp.next = p1;
					break;
				}
			}
		}
		
		return result;
	}
	public static void main(String[] args) {
		LinkedListNode list1 = new LinkedListNode();
		LinkedListNode list2 = new LinkedListNode();
		
		list1.appendToTail(7);
		list1.appendToTail(1);
		list1.appendToTail(6);
		
		list2.appendToTail(5);
		list2.appendToTail(9);
		list2.appendToTail(4);
		list2.appendToTail(2);
		list2.appendToTail(2);
		
		Question5 test = new Question5();
		test.sumLists(list1, list2).printList();
	}

}
