package com.jmsim.chapter6;

public class Question1 {
	
	public void heavyPill(Bottle[] pills){
		double normal = pills.length * (pills.length+1) / 2.0;
		double sum = 0;
		for(int idx = 1 ; idx < pills.length+1 ; idx++){
			sum += idx*(pills[idx-1].getPill().weight);
		}
		
		System.out.println("HEAVY BOTTLE IS :"+(int)((sum-normal)*10-1) +"-th bottle");
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bottle bottles[] = new Bottle[20];
		for(int i = 0 ; i < 19 ; i++){
			bottles[i] = new Bottle(1.0);
		}
		bottles[19] = new Bottle(1.1);
		
		Question1 test = new Question1();
		test.heavyPill(bottles);
	}

}
class Bottle{
	public double pillWeight;
	public Bottle(double pillWeight){
		this.pillWeight = pillWeight;
	}
	public Pill getPill(){
		return new Pill(this.pillWeight);
	}
}
class Pill{
	public double weight;
	public Pill(double weight){
		this.weight = weight;
	}
}