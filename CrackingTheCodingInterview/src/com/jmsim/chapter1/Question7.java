package com.jmsim.chapter1;

public class Question7 {
	public int[][] rotateMatrix(int[][] img){
		int N = img.length;
//		System.out.println("N :"+N);

		for(int row = 0 ; row < (N/2) ; row++){
			for(int col = 0 ; col < (N/2) ; col++){

				int val1 = img[row][col];
				int val2 = img[col][N-row-1];
				int val3 = img[N-row-1][N-col-1];
				int val4 = img[N-col-1][row];
				
				
				img[col][N-row-1] = val1;
				img[N-row-1][N-col-1] = val2;
				img[N-col-1][row] = val3;
				img[row][col] = val4;

			}
		}
		
		if(N%2 != 0){
			for(int row = 0 ; row < (N/2) ; row++){
				int col = (N/2);
				int val1 = img[row][col];
				int val2 = img[col][N-row-1];
				int val3 = img[N-row-1][N-col-1];
				int val4 = img[N-col-1][row];
				
				img[col][N-row-1] = val1;
				img[N-row-1][N-col-1] = val2;
				img[N-col-1][row] = val3;
				img[row][col] = val4;
			}
		}
		return img;	
	}
	
	public static void main(String[] args){
		int img[][] = new int[5][5];
		for(int i = 0 ; i < 5 ; i++){
			for(int j = 0 ; j < 5 ; j++){
				img[i][j] = i*5 + j;
			}
		}
		
		for(int i = 0 ; i < 5 ; i++){
			for(int j = 0 ; j < 5 ; j++){
				System.out.print(img[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println();
		Question7 test = new Question7();
		img = test.rotateMatrix(img);
		
		for(int i = 0 ; i < 5 ; i++){
			for(int j = 0 ; j < 5 ; j++){
				System.out.print(img[i][j]+" ");
			}
			System.out.println();
		}
	}
}
