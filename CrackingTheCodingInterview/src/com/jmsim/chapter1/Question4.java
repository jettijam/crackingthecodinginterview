package com.jmsim.chapter1;

public class Question4 {
	public boolean palindromePermutation(String input){
		StringBuilder sb = new StringBuilder();
		for(int i = 0 ; i < input.length() ; i++){
			if(input.charAt(i) != ' ')
				sb.append(input.charAt(i));
		}
		String str = sb.toString();
		System.out.println(str);
		boolean oddAppear = false;
		for(int i = 0 ; i < str.length() ; i++){
			char c = str.charAt(i);
			int cnt = 0;
			for(int j = 0 ; j < str.length() ; j++){
				if(str.charAt(j) == c)
					cnt++;
			}
			System.out.println(str.charAt(i)+" "+cnt);
			if(cnt%2 == 1){
				if(str.length() % 2 == 0){
					return false;
				}
				if(oddAppear)
					return false;
				oddAppear = true;
			}
		}
		return true;
	}
	
	public boolean palindromePermutation2(String input){
		StringBuilder sb = new StringBuilder();
		for(int i = 0 ; i < input.length() ; i++){
			if(input.charAt(i) != ' ')
				sb.append(input.charAt(i));
		}
		input = sb.toString();
		int charMap[] = new int[256];
		for(int i = 0 ; i < input.length() ; i++){
			int c = input.charAt(i);
			charMap[c]++;
		}
		boolean oddAppear = false;
		for(int i = 0 ; i < 256 ; i++){
			if(charMap[i]%2 != 0){
				if(oddAppear)
					return false;
				oddAppear = true;
			}
		}
		return true;
	}
	public static void main(String[] args) {
		Question4 test = new Question4();
		System.out.println(test.palindromePermutation2("tac   at"));
	}

}
