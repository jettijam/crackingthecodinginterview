package com.jmsim.chapter1;

public class Question6 {
	public String stringCompression(String str){
		StringBuilder sb = new StringBuilder();
		int idx = 0;
		char c = str.charAt(idx);
		while(idx < str.length()){
			int cnt = 0;
			while(idx < str.length() && 
					c == str.charAt(idx)){
				cnt++;
				idx++;
			}
			sb.append(c);
			sb.append(cnt);
			if(idx >= str.length())
				break;
			c = str.charAt(idx);
		}

		String result = sb.toString();

		if(result.length() > str.length())
			return str;
		return result;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Question6 test = new Question6();
		System.out.println(test.stringCompression("aabbbccDDccc"));
	}

}
