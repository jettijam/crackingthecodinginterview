package com.jmsim.chapter1;

public class Question9 {
	public boolean isSubstring(String s1, String s2){
		return s1.contains(s2);
	}
	
	public boolean stringRotation(String s1, String s2){
		if(s1.length() != s2.length())
			return false;
		StringBuilder sb = new StringBuilder(s2);
		sb.append(s2);
		s2 = sb.toString();
		return isSubstring(s2, s1);
	}
	
	public static void main(String[] args){
		String s1 = "apple";
		String s2 = "pleapap";
		Question9 test = new Question9();
		System.out.println(test.stringRotation(s1, s2));
	}
}
