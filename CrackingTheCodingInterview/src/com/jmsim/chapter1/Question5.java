package com.jmsim.chapter1;

public class Question5 {
	public boolean oneAway(String oldStr, String newStr){
		if( (newStr.length() - oldStr.length()) > 1 || 
				(newStr.length() - oldStr.length()) < -1 )
			return false;
		
		boolean oneAway = false;
		
		int oldCursor = 0;
		int newCursor = 0;
		for(oldCursor = 0 ; oldCursor < oldStr.length() ; oldCursor++){
			if(newCursor >= newStr.length()){
				break;
			}
				
			char oldChar = oldStr.charAt(oldCursor);
			char newChar = newStr.charAt(newCursor);
			if(oldChar == newChar){
				newCursor++;
				continue;
			}
			if(!oneAway){
				oneAway = true;
			}else{
				return false;
			}
				
			if(newCursor+1 < newStr.length() &&
					oldChar == newStr.charAt(newCursor+1)){
				
				newCursor++;
				continue;
				
			}
			if(oldCursor+1 < oldStr.length() &&
					oldStr.charAt(oldCursor+1) == newChar){
				oldCursor++;
				continue;
			}
			
			
		}
		return true;

	}
	public static void main(String[] args){
		Question5 test = new Question5();
		System.out.println(test.oneAway("pale", "paleee"));
	}
}
