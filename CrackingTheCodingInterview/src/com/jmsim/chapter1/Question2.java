package com.jmsim.chapter1;

public class Question2 {
	boolean checkPermutation(String str1, String str2){
		if(str1.length() != str2.length())
			return false;
		for(int i = 0 ; i < str1.length() ; i ++){
			String str1_sub = String.valueOf(str1.charAt(i));
			System.out.println(">>str1_sub:"+str1_sub);
			if(!str2.contains(str1_sub))
				return false;
			str2=str2.replace(str1_sub, "");
			System.out.println(">>str2    :"+str2);

		}
		return true;
	}
	
	boolean checkPermutation2(String str1, String str2){
		if(str1.length() != str2.length())
			return false;
		int charMap1[] = new int[256]; //Extended ascii
		int charMap2[] = new int[256];
		for(int i = 0 ; i < str1.length() ; i++){
			charMap1[str1.charAt(i)] ++;
			charMap2[str2.charAt(i)] ++;
		}
		
		for(int i = 0 ; i < 256 ; i++){
			if(charMap1[i] != charMap2[i])
				return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		Question2 test = new Question2();
		System.out.println(test.checkPermutation2("ABCD", "BCDA"));

	}

}
