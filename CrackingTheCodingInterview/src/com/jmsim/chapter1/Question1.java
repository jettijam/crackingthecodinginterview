package com.jmsim.chapter1;

public class Question1 {
	/***
	 * Iterate through the given string to find whether the given string is unique or not
	 * @param str
	 * @return true if the given string has all unique characters. 
	 */
	public boolean isUnique(String str){
		for(int i = 0 ; i < str.length() ; i++){
			for(int j = i+1 ; j < str.length(); j++){
				if(str.charAt(i) == str.charAt(j))
					return false;
			}
		}
		return true;
	}
	
	public boolean isUnique2(String str){
		boolean charMap[] = new boolean[256];
		
		for(int i = 0 ; i < str.length() ; i++){
			int c = str.charAt(i);
			if(charMap[c])
				return false;
			charMap[c] = true;
		}
		return true;
	}
	public static void main(String[] args) {
		Question1 test = new Question1();
		System.out.println(test.isUnique2("ABC Defgd"));
	}

}
