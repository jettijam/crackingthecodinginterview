package com.jmsim.chapter4;

import java.util.ArrayList;

public class Question7 {
	public ArrayList<String> buildOrder(ArrayList<String> projects, ArrayList<Pair> dependencies){
		ArrayList<Node> nodes = new ArrayList<Node>();
		for(String s : projects){
			nodes.add(new Node(s));
		}
		
		//FIND ROOT
		ArrayList<Node> rootCandi = new ArrayList<Node>();
		rootCandi.addAll(nodes);
		for(Pair p : dependencies){
			for(Node n : nodes){
				if(n.name.equals(p.second)){
					if(rootCandi.contains(n));
						rootCandi.remove(n);
				}
			}
			
		}
		System.out.println("NODE CANDI SIZE :"+rootCandi.size());
		if(rootCandi.size() < 1){
			return null;
		}
		// Create dependencies
		for(Pair p : dependencies){
			Node parent = null;
			Node child = null;
			for(Node n : nodes){
				if(n.name.equals(p.first)){
					parent = n;
				}
				if(n.name.equals(p.second)){
					child = n;
				}
			}
			parent.children.add(child);
		}
		
		ArrayList<String> result = new ArrayList<String>();
		nodeToList(rootCandi, result);
		
		return result;
	}
	
	private void nodeToList(ArrayList<Node> nodes, ArrayList<String> result){
		for(Node n : nodes){
			if(!result.contains(n.name)){
				result.add(n.name);
			}
		}
		ArrayList<Node> newNodes = new ArrayList<Node>();
		for(Node n : nodes){
			newNodes.addAll(n.children);
		}
		if(!newNodes.isEmpty())
			nodeToList(newNodes, result);
	}
	
	public static void main(String[] args){
		Question7 test = new Question7();
		ArrayList<String> projects = new ArrayList<String>();
//		{"a", "b", "c", "d", "e", "f"};
		projects.add("a");
		projects.add("b");
		projects.add("c");
		projects.add("d");
		projects.add("e");
		projects.add("f");
		
		ArrayList<Pair> dependencies = new ArrayList<Pair>();
		dependencies.add(new Pair("a", "d"));
		dependencies.add(new Pair("f", "b"));
		dependencies.add(new Pair("b", "d"));
		dependencies.add(new Pair("f", "a"));
		dependencies.add(new Pair("d", "c"));
		
		ArrayList<String> result = test.buildOrder(projects, dependencies);
		for(String s : result){
			System.out.println(s);
		}
	}
}
class Pair{
	String first;
	String second;
	
	public Pair(String f, String s){
		first = f;
		second = s;
	}
}