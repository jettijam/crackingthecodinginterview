package com.jmsim.chapter4;

import java.util.Random;

public class GraphBuilder {
	public static Graph randomGraph(int num){
		Graph result = new Graph();
		Random random = new Random();
		result.nodes = new Node[num];
		for(int i = 0 ; i < num ; i++){
			result.nodes[i] = new Node(String.valueOf(i));
		}
		
		
		for(int i = 0 ; i < (int)num*1 ; i++){
			int src = random.nextInt(num);
			int dst = random.nextInt(num);
			while(src==dst || result.nodes[src].isChild(result.nodes[dst])){
				dst = random.nextInt(num);
			}
			result.nodes[src].children.add(result.nodes[dst]);
			result.nodes[dst].children.add(result.nodes[src]);
		}
		
		return result;
	}
}
