package com.jmsim.chapter4;

import java.util.ArrayList;

public class Question12 {
	int gResult = 0;
	public int pathsToSum(MyTree tree, int val){
		dfsPathsToSum(tree.root, val);
		
		return gResult;
	}
	
	public ArrayList<Integer> dfsPathsToSum(Node node, int val){
		ArrayList<Integer> result = new ArrayList<Integer>();
		result.add(Integer.valueOf(node.name));
		
		for(Node c : node.children){
			ArrayList<Integer> tmpList = dfsPathsToSum(c, val);
			for(Integer i : tmpList){
				result.add(i+result.get(0));
			}
		}
		System.out.println("### Node : "+node.name+"###");
		for(Integer i : result){
			if(i == val)
				gResult++;
			System.out.println(i);
		}
		System.out.println();
		return result;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyTree t1 = new MyTree(new Node("2"));
		t1.root.children.add(new Node("1"));
		t1.root.children.add(new Node("5"));
		t1.root.children.get(1).children.add(new Node("4"));
		t1.root.children.get(1).children.add(new Node("6"));
		
		Question12 test = new Question12();
		System.out.println(test.pathsToSum(t1,  9));
	}

}
