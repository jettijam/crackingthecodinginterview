package com.jmsim.chapter4;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class Question10 {
	public boolean checkSubTree(MyTree t1, MyTree t2){
		Set<Node> marked = new HashSet<Node>();
		Queue<Node> q = new LinkedList<Node>();
		q.add(t1.root);
		marked.add(t1.root);
		
		while(!q.isEmpty()){
			Node r = q.poll();
			if(r.name.equals(t2.root.name)){
				if(dfsCheckSubTree(new MyTree(r), t2))
					return true;
			}
			for(Node c : r.children){
				if(marked.contains(c)){
					continue;
				}
				marked.add(c);
				q.add(c);
			}
		}
		
		return false;
	}
	
	public boolean dfsCheckSubTree(MyTree t1, MyTree t2){
		System.out.println("DFS Checking : "+t1.root.name+" vs. "+t2.root.name);
		if(!t1.root.name.equals(t2.root.name))
			return false;
		if(t1.root.children.size() != t2.root.children.size())
			return false;
		for(int idx = 0 ; idx < t1.root.children.size() ; idx++){
			if(!dfsCheckSubTree(new MyTree(t1.root.children.get(idx)), new MyTree(t2.root.children.get(idx)) )){
				return false;
			}
		}
		return true;
	}
	public static void main(String[] args) {
		MyTree t1 = new MyTree(new Node("2"));
		t1.root.children.add(new Node("1"));
		t1.root.children.add(new Node("5"));
		t1.root.children.get(1).children.add(new Node("4"));
		t1.root.children.get(1).children.add(new Node("6"));

//		MyTree t2 = new MyTree(t1.root.children.get(1));
		MyTree t2 = new MyTree(new Node("5"));
		t2.root.children.add(new Node("4"));
		t2.root.children.add(new Node("6"));
		
		Question10 test = new Question10();
		System.out.println(test.checkSubTree(t1, t2));
	}

}
