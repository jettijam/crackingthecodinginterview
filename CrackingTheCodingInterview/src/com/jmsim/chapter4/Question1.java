package com.jmsim.chapter4;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

public class Question1 {
	
	
	
	public boolean routeBetweenNodes(Node n1, Node n2){
		Queue<Node> q1 = new LinkedList<Node>();
		Queue<Node> q2 = new LinkedList<Node>();
		
		Set<Node> marked1 = new HashSet<Node>();
		Set<Node> marked2 = new HashSet<Node>();
		
		marked1.add(n1);
		marked2.add(n2);
		q1.add(n1);
		q2.add(n2);
		while(!q1.isEmpty() || !q2.isEmpty()){
			if(!q1.isEmpty()){
				
			Node r1 = q1.poll();
			if(r1 == null){
				System.out.println("WHY!!2");
			}
//			System.out.println(r.name+ " VISITED");
			ArrayList<Node> children1 = r1.children;
			for(Node child1 : children1){
				if(!marked1.contains(child1)){
				    marked1.add(child1);
				    q1.add(child1);
//				    System.out.println(n.name +" enqueued : "+q1.size());
				}else{
					if(marked2.contains(child1))
						return true;
//					System.out.println(n.name + " is already visited");
				}
			}
			}
			
			if(!q2.isEmpty()){
			Node r2 = q2.poll();
//			System.out.println(r.name+ " VISITED");
			if(r2 == null){
				System.out.println("WHY!!2");
			}
			ArrayList<Node> children2 = r2.children;
			for(Node child2 : children2){
				if(!marked2.contains(child2)){
				    marked2.add(child2);
				    q2.add(child2);
//				    System.out.println(n.name +" enqueued : "+q1.size());
				}else{
					if(marked1.contains(child2))
						return true;
				}
			}
			}
		}
		
		return false;
	}
	
	public static void main(String[] args){
		Question1 test = new Question1();
		
		Graph testGraph = GraphBuilder.randomGraph(10);
		testGraph.printGraph();
		
		System.out.println(test.routeBetweenNodes(testGraph.nodes[0], testGraph.nodes[9]));
	}
}
