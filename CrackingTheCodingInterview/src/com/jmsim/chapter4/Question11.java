package com.jmsim.chapter4;

import java.util.ArrayList;
import java.util.Random;

public class Question11 {
	public Node randomNode(MyTree tree){
		ArrayList<Node> nodeList = new ArrayList<Node>();
		dfsNodeList(tree.root, nodeList);
	
		Random r = new Random();
		return nodeList.get(r.nextInt(nodeList.size()));
	}
	
	public void dfsNodeList(Node node, ArrayList<Node> list){
		list.add(node);
		for(Node c : node.children){
			dfsNodeList(c, list);
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyTree t1 = new MyTree(new Node("2"));
		t1.root.children.add(new Node("1"));
		t1.root.children.add(new Node("5"));
		t1.root.children.get(1).children.add(new Node("4"));
		t1.root.children.get(1).children.add(new Node("6"));
		
		Question11 test = new Question11();
		System.out.println(test.randomNode(t1).name);

	}

}
