package com.jmsim.chapter4;

import java.util.ArrayList;

public class Question4 {
	public boolean checkBalanced(MyTree tree){
		if(tree.root.children.size() == 0){
			return true;
		}
		MyTree left = null;
		MyTree right = null;
		
		if(tree.root.children.size() > 0 && tree.root.children.get(0) != null){
			left = new MyTree(tree.root.children.get(0));
		}
		if(tree.root.children.size() > 1 && tree.root.children.get(1) != null){
			right = new MyTree(tree.root.children.get(1));
		}
		
		int left_size = 0;
		int right_size = 0;
		if(left != null){
			left_size = getSize(left);
		}
		if(right != null){
			right_size = getSize(right);
		}
		System.out.println("LEFT SIZE :"+left_size);
		System.out.println("RGHT SIZE :"+right_size);
		if( (left_size - right_size) > 1 || (left_size - right_size) < -1 ){
			return false;
		}
		return true;
	}
	
	public int getSize(MyTree tree){
		if(tree.root.children.size() == 0){
			return 1;
		}
		int left = 0;
		int right = 0;
		if(tree.root.children.size() > 0 && tree.root.children.get(0) != null){
			left = getSize(new MyTree(tree.root.children.get(0)));
		}
		if(tree.root.children.size() > 1 && tree.root.children.get(1) != null){
			right = getSize(new MyTree(tree.root.children.get(1)));
		}
		System.out.println("getSize @"+tree.root.name+" : "+left+", "+right);
		return left + right+1;
	}
	
	public static void main(String[] args){
		int arr[] = new int[6];
		arr[0] = 2;
		arr[1] = 4;
		arr[2] = 6;
		arr[3] = 8;
		arr[4] = 10;
		arr[5] = 20;
		Question2 q2 = new Question2();
		MyTree tree = q2.minimalTree(arr);
		tree.root.children.get(1).children = new ArrayList<Node>();
		q2.dfs(tree.root, 0);
		
		Question4 test = new Question4();
		System.out.println(test.checkBalanced(tree));
	}
}
