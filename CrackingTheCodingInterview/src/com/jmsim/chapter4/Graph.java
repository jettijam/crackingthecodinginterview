package com.jmsim.chapter4;

public class Graph {
	public Node[] nodes;
	
	public void printGraph(){
		for(int i = 0 ; i < nodes.length ; i++){
			System.out.println(nodes[i].name);
			for(int j = 0 ; j < nodes[i].children.size() ; j++){
				System.out.println("    >>"+nodes[i].children.get(j).name);
			}
		}
	}
}
