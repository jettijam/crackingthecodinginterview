package com.jmsim.chapter4;

public class Question2 {
	
	public void dfs(Node node, int depth){
		for(int i = 0 ; i < depth ; i++)
			System.out.print("  ");
		System.out.println(node.name);
		for(Node c : node.children){
			dfs(c, depth+1);
		}
		
	}
	
	public MyTree minimalTree(int[] arr){
		System.out.println("input arr length : "+arr.length);
		if(arr.length == 1){
			return new MyTree(new Node(String.valueOf(arr[0])));
		}
		int mid = arr.length/2;
		
		Node root = new Node(String.valueOf(arr[mid]));
		MyTree result = new MyTree(root);
		
		int left[] = new int[arr.length/2];
		int right[] = new int[arr.length - left.length - 1];
		
		for(int i = 0 ; i < left.length ; i++){
			left[i] = arr[i];
		}
		for(int i = mid+1 ; i < arr.length ; i++){
			right[i-mid-1] = arr[i];
		}
		if(left.length > 0)
			result.root.addChild(minimalTree(left).root);
		if(right.length > 0)
			result.root.addChild(minimalTree(right).root);
		return result;
	}
	
	public static void main(String[] args){
		int arr[] = new int[6];
		arr[0] = 2;
		arr[1] = 4;
		arr[2] = 6;
		arr[3] = 8;
		arr[4] = 10;
		arr[5] = 20;
		Question2 test = new Question2();
		test.dfs( test.minimalTree(arr).root, 0 );
		
	}
}
