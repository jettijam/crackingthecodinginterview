package com.jmsim.chapter4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Question3 {
	public ArrayList<ArrayList<Node>> listOfDepth(MyTree tree){
		ArrayList<ArrayList<Node>> result = new ArrayList<ArrayList<Node>>();
		
		HashMap<Integer, ArrayList<Node>> map = new HashMap<Integer, ArrayList<Node>>();
		listOfDepth_real(tree.root, 0, map);
		
		Iterator<Integer> itr = map.keySet().iterator();
		while(itr.hasNext()){
			int depth = itr.next();
			result.add(map.get(depth));
		}
		
		return result;
	}
	public void listOfDepth_real(Node node, int depth, HashMap<Integer, ArrayList<Node>> result){
		if(node == null)
			return;
		if(!result.keySet().contains(depth)){
			result.put(depth, new ArrayList<Node>());
		}
		result.get(depth).add(node);
		for(Node c : node.children){
			listOfDepth_real(c, depth+1, result);
		}
		return;
	}
	public static void main(String[] args) {
		int arr[] = new int[6];
		arr[0] = 2;
		arr[1] = 4;
		arr[2] = 6;
		arr[3] = 8;
		arr[4] = 10;
		arr[5] = 20;
		Question2 test = new Question2();
		MyTree tree = test.minimalTree(arr);
		
		Question3 test3 = new Question3();
		ArrayList<ArrayList<Node>> result = test3.listOfDepth(tree);
		for(int i = 0 ; i < result.size() ; i++){
			System.out.println("### depth : "+i);
			for(int j = 0 ; j < result.get(i).size() ; j++){
				System.out.print(result.get(i).get(j).name+" ");
			}
			System.out.println();
		}
	}

}
