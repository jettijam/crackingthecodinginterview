package com.jmsim.chapter4;

public class Question6 {
	public Node successor(MyTree tree, Node node){
		if(tree.root == node){
			if(tree.root.children.size() < 2){
				return tree.root;
			}
			return findSuccessor(new MyTree(tree.root.children.get(1)));
		}
		
		Node left = null;
		Node right = null;
		
		if(tree.root.children.size() > 0 && tree.root.children.get(0)!=null){
			left = successor(new MyTree(tree.root.children.get(0)), node);
		}
		if(left != null)
			return left;
		if(tree.root.children.size() > 1 && tree.root.children.get(1)!=null){
			right = successor(new MyTree(tree.root.children.get(1)), node);
		}
		return right;
	}
	
	public Node findSuccessor(MyTree tree){
		if(tree.root.children.size() < 1){
			return tree.root;
		}
		return findSuccessor(new MyTree(tree.root.children.get(0)));
	}
	
	public static void main(String[] args) {
		int arr[] = new int[6];
		arr[0] = 2;
		arr[1] = 4;
		arr[2] = 6;
		arr[3] = 8;
		arr[4] = 10;
		arr[5] = 20;
		Question2 q2 = new Question2();
		MyTree tree = q2.minimalTree(arr);
		q2.dfs(tree.root, 0);
		Question6 test = new Question6();
		System.out.println(test.successor(tree, tree.root.children.get(0)).name);

	}

}
