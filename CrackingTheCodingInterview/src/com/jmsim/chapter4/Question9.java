package com.jmsim.chapter4;

import java.util.ArrayList;

public class Question9 {
	public ArrayList<ArrayList<String>> BSTSequence(MyTree tree){
		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
		
		result = recurBSTSequence(tree);
		return result;
	}
	
	public ArrayList<ArrayList<String>> recurBSTSequence(MyTree tree){
		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
		
		if(tree.root.children.isEmpty()){
			ArrayList<String> tmp = new ArrayList<String>();
			tmp.add(tree.root.name);
			result.add(tmp);
			return result;
		}
		
		
		ArrayList<ArrayList<String>> left_result = new ArrayList<ArrayList<String>>();
		if(tree.root.children.size() > 0){
			left_result = recurBSTSequence(new MyTree(tree.root.children.get(0)));
		}
		
		ArrayList<ArrayList<String>> right_result = new ArrayList<ArrayList<String>>();
		if(tree.root.children.size() > 1){
			right_result = recurBSTSequence(new MyTree(tree.root.children.get(1)));
		}
		
		for(ArrayList<String> l : left_result){
			for(ArrayList<String> r : right_result){
				recurWeave(l, r, result, new ArrayList<String>());
			}
		}
		
		for(ArrayList<String> item : result){
			item.add(0, tree.root.name);
		}
		return result;
	}
	
	public void recurWeave(ArrayList<String> list1, ArrayList<String> list2, ArrayList<ArrayList<String>> results, ArrayList<String> prefix){
		if(list1.size() == 0 || list2.size() == 0){
			ArrayList<String> result = new ArrayList<String>();
			result.addAll(prefix);
			result.addAll(list1);
			result.addAll(list2);
			results.add(result);
			return;
		}
		String headFirst = list1.get(0);
		list1.remove(0);
		prefix.add(headFirst);
		recurWeave(list1, list2, results, prefix);
		prefix.remove(prefix.size()-1);
		list1.add(0, headFirst);
		
		String headSecond = list2.get(0);
		list2.remove(0);
		prefix.add(headSecond);
		recurWeave(list1, list2, results, prefix);
		prefix.remove(prefix.size()-1);
		list2.add(0, headSecond);
				
	}
	
	public static void main(String[] args){
		MyTree tree = new MyTree(new Node("2"));
		tree.root.children.add(new Node("1"));
		tree.root.children.add(new Node("5"));
		tree.root.children.get(1).children.add(new Node("4"));
		tree.root.children.get(1).children.add(new Node("6"));
		Question2 q2 = new Question2();
		q2.dfs(tree.root, 0);
		
		Question9 test = new Question9();
		ArrayList<ArrayList<String>> result = test.BSTSequence(tree);
		for(ArrayList<String> r : result){
			System.out.println("#############################");
			for(String s : r){
				System.out.print(s +" ");
			}
			System.out.println();
		}
		
		
		
	}
}
