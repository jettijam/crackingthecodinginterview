package com.jmsim.chapter4;

import java.util.ArrayList;

public class Node {
	public String name;
	public ArrayList<Node> children = new ArrayList<Node>();
	public Node(){}
	public Node(String name){this.name=name;}
	public boolean isChild(Node node){
		return children.contains(node);
	}
	
	public void addChild(Node node){
		this.children.add(node);
//		node.children.add(this);
	}
}
