package com.jmsim.chapter8;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Question4 {
		
	public Set<ArrayList<Integer>> powerSet(ArrayList<Integer> set){
		Set<ArrayList<Integer>> result = new HashSet<ArrayList<Integer>>();
		recurPowerSet(set, result);
		return result;
	}
	
	
	
	public void recurPowerSet(ArrayList<Integer> set, Set<ArrayList<Integer>> result){
		
		Iterator<Integer> inner = set.iterator();
		System.out.print("PARAM SET { ");
		while(inner.hasNext()){
			System.out.print(inner.next() +" ");
		}
		System.out.println("} ");

		if(set.size() == 1) {
			result.add(set);
			return;
		}
		
		int window = 1;
		for(window = 1 ; window < set.size() ; window ++) {
			int start = 0;
			for(start = 0 ;  start < set.size() ; start++) {
				ArrayList<Integer> picked = new ArrayList<Integer>();
				for(int c = 0 ; c < window ; c++) {
					int cursor = c + start;
					if(cursor >= set.size()) {
						cursor -= set.size();
					}
					picked.add(set.get(cursor));
				}
				recurPowerSet(picked, result);
			}
		}
		
		
		result.add(set);
		return;
		
//		Iterator<Integer> itr = set.iterator();
//		int idx = 0;
//		Set<Integer> leftSet = new HashSet<Integer>();
//		Set<Integer> rightSet = new HashSet<Integer>();
//		while(itr.hasNext()){
//			if(idx++ < size/2){
//				leftSet.add(itr.next());
//			}else{
//				rightSet.add(itr.next());
//			}
//		}
//		
//		powerSet(leftSet, result);
//		powerSet(rightSet, result);
//		result.add(set);
//		return;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> set = new ArrayList<Integer>();
		set.add(1);
		set.add(2);
		set.add(3);
		
		Question4 test = new Question4();
		Set<ArrayList<Integer>> result = test.powerSet(set);
		
		Iterator<ArrayList<Integer>> itr = result.iterator();
		while(itr.hasNext()){
			Iterator<Integer> inner = itr.next().iterator();
			System.out.print("{ ");
			while(inner.hasNext()){
				System.out.print(inner.next() +" ");
			}
			System.out.println("} ");
		}
	}

}
