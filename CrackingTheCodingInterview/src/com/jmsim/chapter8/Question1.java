package com.jmsim.chapter8;

public class Question1 {
	
	int recurStep(int step, int cur, int num){
		int res = 0;
		if( cur + step == num ){
			return 1;
		}
		if( cur + step > num ){
			return 0;
		}
		res += recurStep(1, cur+step, num);
		res += recurStep(2, cur+step, num);
		res += recurStep(3, cur+step, num);
		return res;
	}
	
	public int tripleStep(int num){
		int res = recurStep(1, 0, num);
		res += recurStep(2, 0, num);
		res += recurStep(3, 0, num);
		return res;
	}
	
	
	
	public static void main(String[] args){
		Question1 test = new Question1();
		System.out.println(test.tripleStep(4));
	}
}
