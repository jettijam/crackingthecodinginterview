package com.jmsim.chapter8;

import java.util.ArrayList;

public class Question9 {
	public ArrayList<String> parens(int num){
		ArrayList<String> result = new ArrayList<String>();
		recurParens(num, num, "", result);
		return result;
	}
	
	public void recurParens(int lNum, int rNum, String pred, ArrayList<String> result){
		if(lNum == 0){
			for(int i = 0 ; i < rNum ; i++){
				pred += ")";
				
			}
			result.add(pred);
			return;
		}
		
		recurParens(lNum-1, rNum, pred+"(", result);
		if(rNum>0 && rNum > lNum){
			recurParens(lNum, rNum-1, pred+")", result);
		}
	}
	public static void main(String[] args) {
		Question9 test = new Question9();
		ArrayList<String> result = test.parens(3);
		for(String s : result) {
			System.out.println(s);
		}
	}

}
