package com.jmsim.chapter8;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Question8 {
	public Set<String> permutation(String str){
		Set<String> result = new HashSet<String>();
		recurPerm("", str, result);
		return result;
	}

	public void recurPerm(String pre, String rem, Set<String> result) {
		if(rem.equals("")) {
			result.add(pre);
			return;
		}
		for(int i = 0 ; i < rem.length() ; i++) {
			recurPerm(pre + rem.charAt(i), rem.substring(0, i)+rem.substring(i+1, rem.length()), result);

		}
		return;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Question8 test = new Question8();
		Set<String> result = test.permutation("AAB");
		for(String s : result) {
			System.out.println(s);
		}
	}

}
