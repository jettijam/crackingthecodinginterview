package com.jmsim.chapter8;

public class Question10 {
	public void paintFill(Pixel[][] img, int x, int y, Pixel color) {
		Pixel orig = new Pixel(img[y][x].r, img[y][x].g, img[y][x].b);
		
		recurPaintFill(img, orig, x, y, color);
		
	}
	
	public void recurPaintFill(Pixel[][] img, Pixel orig, int x, int y, Pixel color) {
		Pixel cur = img[y][x];
		if(!cur.equals(orig)) {
			return;
		}
		img[y][x].r = color.r;
		img[y][x].g = color.g;
		img[y][x].b = color.b;
		
		if(y > 0) {
			recurPaintFill(img, orig, x, y-1, color);
		}
		if(x > 0) {
			recurPaintFill(img, orig, x-1, y, color);
		}
		if(y < img.length-1) {
			recurPaintFill(img, orig, x, y+1, color);
		}
		if(x < img[0].length -1) {
			recurPaintFill(img, orig, x+1, y, color);
		}
	}
	
	public void printImage(Pixel[][] img) {
		for(int y = 0 ; y < img.length ; y++) {
			for(int x = 0 ; x < img[0].length ; x++) {
				System.out.print(img[y][x] + " ");
			}
			System.out.println();
		}
		System.out.println();

	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Pixel[][] img = new Pixel[3][3];
		for(int y = 0 ; y < img.length ; y++) {
			for(int x = 0 ; x < img[0].length ; x++) {
				img[y][x] = new Pixel(0, 0, 0);
			}
		}
		
		img[1][1].r=255;
		img[1][1].g=255;
		img[1][1].b=255;
		
		img[0][2].r=255;
		img[0][2].g=255;
		img[0][2].b=255;
		
		img[2][2].r=255;
		img[2][2].g=255;
		img[2][2].b=255;
		
		
		Question10 test = new Question10();
		test.printImage(img);
		
		test.paintFill(img, 0, 0, new Pixel(1, 1, 1));
		test.printImage(img);

	}
	
	

}

class Pixel{
	int r=0;
	int g=0;
	int b=0;
	public Pixel(int r, int g, int b){
		this.r = r;
		this.g= g;
		this.b = b;
	}
	
	@Override
	public boolean equals(Object p){
		if(p.getClass()!= Pixel.class){
			return false;
		}
		Pixel pp = (Pixel)p;
		return (this.r == pp.r && this.g==pp.g && this.b == pp.b);
	}
	
	@Override
	public String toString(){
		return String.format("(%3d,%3d,%3d)", this.r, this.g, this.b);
	}
}
