package com.jmsim.chapter8;

import java.util.ArrayList;

public class Question2 {
	public ArrayList<String> robotInGrid(boolean grid[][]){ // if grid[y][x] == false : it means it is not reachable
		ArrayList<String> path = new ArrayList<String>();
		if(recurPath(0, 0, grid, path)){
			return path;
		}
		return path;
	}
	public boolean recurPath(int x, int y, boolean grid[][], ArrayList<String> path){
//		System.out.print(String.format("@(%d,%d) : ", x, y));
		if( x == grid[0].length-1 && y == grid.length -1){
//			System.out.println("1 true");
			return true;
		}
		
		if( !grid[y][x] ){
//			System.out.println("2 false");
			return false;
		}
		
		if( x+1 < grid[0].length ){
			if(recurPath(x+1, y, grid, path)){
				path.add(0, "r");
//				System.out.println("3 true");
				return true;
			}
				
		}
		if(y+1<grid.length){
			if(recurPath(x, y+1, grid, path)){
//				System.out.println("4 true");
				path.add(0, "d");
				return true;
			}
		}
//		System.out.println("5 false");
		return false;
	}
	
	public void printGrid(boolean grid[][]){
		for(int y = 0 ; y < grid.length ; y++){
			for(int x = 0 ; x < grid[0].length ; x++){
				if(grid[y][x]){
					System.out.print(" -");
				}else{
					System.out.print(" *");
				}
			}
			System.out.println();
		}
		System.out.println();
	}
	
	public static void main(String[] args){
		boolean grid[][] = new boolean[5][5];
		for(int y = 0 ; y < grid.length ; y++){
			for(int x = 0 ; x < grid[0].length ; x++){
				grid[y][x] = true;
			}
		}
		
		grid[0][1] = false;
		grid[1][1] = false;
		
		Question2 test = new Question2();
		
		test.printGrid(grid);
		
		ArrayList<String> path = test.robotInGrid(grid);
		for(String s : path){
			System.out.println(s);
		}
	}
}
