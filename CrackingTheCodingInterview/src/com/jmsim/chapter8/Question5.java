package com.jmsim.chapter8;

public class Question5 {
	public int recursiveMultiply(int n, int k) {
		
		Integer result = recurMultiply(n, k, n);
		return result;
		
		
		
	}
	
	public int recurMultiply(int n, int k, int result) {
		System.out.println(String.format("n, k, result : %d, %d, %d", n, k, result));
		if(k == 0) {
			result=0;
			return result;
		}
		if(k == 1)
			return result;
	
		return recurMultiply(n, k-1, result + n);
		
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Question5 test = new Question5();
		System.out.println(test.recursiveMultiply(5, 3));
	}

}
