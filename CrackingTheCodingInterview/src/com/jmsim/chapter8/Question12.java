package com.jmsim.chapter8;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Question12 {
	Set<Set<Coord>> results = new HashSet<Set<Coord>>();
	int boardSize = 3;
	int queenNum = 2;
	public void eightQueens() {
		
		for(int y = 0 ; y < boardSize ; y ++) {
			for(int x = 0 ; x < boardSize ; x++) {
				Set<Coord> set = new HashSet<Coord>();
				set.add(new Coord(x, y));
				recurEightQueens(set);
			}
		}
		int i=0;
		Iterator<Set<Coord>> itr = results.iterator();
		while(itr.hasNext()) {
			System.out.println("SOLUTION #"+i++);
			Set<Coord> sc = itr.next();
			Iterator<Coord> inner = sc.iterator();
			while(inner.hasNext()) {
				Coord c = inner.next();
				System.out.println(String.format("    (%d,%d)", c.x, c.y));
			}
			
		}
	}
	
	public void recurEightQueens(Set<Coord> coords) {
		if(coords.size() == queenNum) {
			if(!results.contains(coords)) {
				results.add(coords);
			}
			return;
		}
		
		boolean checkBoard[][] = new boolean[boardSize][boardSize];
		for(int y = 0 ; y < boardSize ; y++) {
			for(int x = 0 ; x < boardSize ; x++) {
				checkBoard[y][x] = false;
			}
		}
		
		Iterator<Coord> itr = coords.iterator();
		while(itr.hasNext()) {
			Coord c = itr.next();
			int y = c.y;
			int x = c.x;
			
			for(int i = 0 ; i < boardSize ; i++) {
				checkBoard[y][i] = true;
			}
			for(int i = 0 ; i < boardSize ; i ++) {
				checkBoard[i][x] = true;
			}
			
			int i = y;
			int j = x;
			while(i >= 0 && j >= 0) {
				checkBoard[i--][j--] = true;
			}
			i = y;
			j = x;
			while(i < boardSize && j < boardSize) {
				checkBoard[i++][j++] = true;
			}
			i = y;
			j = x;
			while(i < boardSize && j >= 0) {
				checkBoard[i++][j--] = true;
			}
			
			i = y;
			j = x;
			while(i >= 0 && j < boardSize) {
				checkBoard[i--][j++] = true;
			}
		}
		
		for(int y = 0 ; y < boardSize ; y++) {
			for(int x = 0 ; x < boardSize ; x++) {
				if(!checkBoard[y][x]) {
					Set<Coord> newSet = new HashSet<Coord>();
					newSet.addAll(coords);
					newSet.add(new Coord(x, y));
					recurEightQueens(newSet);
				}
			}
		}
	}
	
	
	
	public void recurEightQueens_old(boolean board[][], int numQueen) {
		if(numQueen == 3) {
			for(int y = 0 ; y < 8 ; y++) {
				for(int x = 0 ; x < 8 ; x++) {
					if(board[y][x]) {
						System.out.print("Q ");
					}else {
						System.out.print("* ");
					}
					
				}
				System.out.println();
			}
			
			System.out.println();
			return;
		}
		
		
		//put queen at valid spot
		boolean checkBoard[][] = new boolean[8][8];
		for(int y = 0 ; y < 8 ; y++) {
			for(int x = 0 ; x < 8 ; x++) {
				checkBoard[y][x] = false;
			}
		}
		
		for(int y = 0 ; y < board.length ; y++) {
			for(int x = 0 ; x < board[0].length ; x++) {
				if(board[y][x]) {
					
					for(int i = 0 ; i < 8 ; i++) {
						checkBoard[y][i] = true;
					}
					for(int i = 0 ; i < 8 ; i ++) {
						checkBoard[i][x] = true;
					}
					
					int i = y;
					int j = x;
					while(i >= 0 && j >= 0) {
						checkBoard[i--][j--] = true;
					}
					i = y;
					j = x;
					while(i < 8 && j < 8) {
						checkBoard[i++][j++] = true;
					}
					
				
					
					
				}
			}
		}
		
		
		for(int y = 0 ; y < 8 ; y++) {
			for(int x = 0 ; x < 8 ; x++) {
				
				if(!checkBoard[y][x]) {
					boolean newBoard[][] = new boolean[8][8];
					
					for(int i = 0 ; i < 8 ; i ++) {
						for(int j = 0 ; j < 8 ; j++) {
							newBoard[i][j] = board[i][j];
						}
					}
					
					newBoard[y][x] = true;
					recurEightQueens_old(newBoard, numQueen+1);
				}
				
			}
		}
		
		
		
		
	}
	
	
	public static void main(String[] args) {
		Question12 test = new Question12();
		test.eightQueens();
	}
}

class Coord{
	int x, y;
	public Coord(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	
	@Override
	public boolean equals(Object c) {
		if(c.getClass()!=Coord.class) {
			return false;
		}
		Coord co = (Coord)c;
		return this.x == co.x & this.y == co.y;
	}
	
	@Override
	public int hashCode() {
		return x*10+y;
	}
}