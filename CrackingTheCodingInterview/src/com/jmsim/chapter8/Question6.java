package com.jmsim.chapter8;

import java.util.Stack;

public class Question6 {
	public void towersOfHanoi(Stack<Integer> src, Stack<Integer> tmp, Stack<Integer> dst) {
		
		recurHanoi(src.size(), src, tmp, dst);
	}
	
	public void recurHanoi(int n, Stack<Integer> src, Stack<Integer> tmp, Stack<Integer> dst) {
		if(n==1) {
			
			dst.push(src.pop());
			return;
		}
		
		recurHanoi(n-1, src, dst, tmp);
		dst.push(src.pop());
		recurHanoi(n-1, tmp, src, dst);
	}
	
	
	public static void main(String[] args) {
		Stack<Integer> src = new Stack<Integer>();
		Stack<Integer> tmp = new Stack<Integer>();
		Stack<Integer> dst = new Stack<Integer>();
		
		src.push(5);
		src.push(4);
		src.push(3);
		src.push(2);
		src.push(1);
		Question6 test = new Question6();
		test.towersOfHanoi(src, tmp, dst);
		
		while(!dst.isEmpty()) {
			System.out.println(dst.pop());
		}
	}
}
