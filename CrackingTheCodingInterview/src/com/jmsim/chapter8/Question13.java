package com.jmsim.chapter8;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;
import java.util.Stack;

public class Question13 {
	
	public int stackOfBox(ArrayList<Box> boxes){
		
		ArrayList<Box> result = recurBox(boxes, new ArrayList<Box>());
		System.out.println("RESULT :");
		for(Box b : result){
			System.out.println(b.toString());
		}
		return this.getStackHeight(result);
	}
	
	public ArrayList<Box> recurBox(ArrayList<Box> input, ArrayList<Box> state){
		//TODO : 현재 state (Stack)의 head보다 작은 애들을 순회하면서 recur
		//TODO : 위에서 돌려받은 stack의 길이를 다 재서 제일 큰거 return
		if(input.size() == 0){
			return state;
		}
		
		Box head = new Box(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
		
		if(state.size() > 0){
			head = state.get(state.size()-1);
		}
		
		int maxSize = Integer.MIN_VALUE;
		ArrayList<Box> result = state;
		
		for(int i = 0 ; i < input.size() ; i++){
			Box cur = input.get(i);
			if(head.biggerThan(cur)){
				
				//TODO : Create newInput (Input - cur)
				ArrayList<Box> newInput = new ArrayList<Box>();
				for(int j = 0 ; j < input.size() ; j++){
					if(input.get(j).equals(cur)){
//						System.out.println("EQUAL!");
						continue;
					}
					newInput.add(input.get(j));
				}
				//TODO : Create newState (State + cur)
				ArrayList<Box> newState = new ArrayList<Box>();
				for(int j = 0 ; j < state.size() ; j++){
					newState.add(state.get(j));
				}
				newState.add(cur);
						
				ArrayList<Box> tmp = recurBox(newInput, newState);
				if( getStackHeight(tmp) > maxSize ){
					maxSize = getStackHeight(tmp);
					result = tmp;
				}
			}	
		}
		return result;
//		
//		if(input.size() == 0){
//			System.out.println("INPUT LEN ZERO");
//			return state;
//		}
//		
//		System.out.println("STATE");
//		for(Box b : input){
//			System.out.println(b.toString());
//		}
//		
//		int maxH = Integer.MIN_VALUE;
//		ArrayList<Box> result = new ArrayList<Box>();
//		
//		if(state.size() == 0){
//			for(Box b : input){
//				ArrayList<Box> newInput = new ArrayList<Box>();
//				for(Box t : input){
//					if(t!=b){
//						newInput.add(t);
//					}
//				}
//				ArrayList<Box> newState = new ArrayList<Box>();
//				newState.add(b);
//				ArrayList<Box> r = recurBox(newInput, newState);
//				if( getStackHeight(r) > maxH ){
//					result = r;
//				}
//			}
//			
//			System.out.print("STATE 0!!  ");
//		}else{
//			
//			for(Box b : input){
//				
//				if( !b.biggerThan(state.get(state.size()-1)) ){
//					ArrayList<Box> newInput = new ArrayList<Box>();
//					for(Box t : input){
//						if(t!=b){
//							newInput.add(t);
//						}
//					}
//					ArrayList<Box> newState = new ArrayList<Box>();
//					if(state.size() > 0){
//						for(Box t : state){
//							newState.add(t);
//						}
//					}
//					newState.add(b);
//					
//					ArrayList<Box> r = recurBox(newInput, newState);
//					if( getStackHeight(r) > maxH ){
//						result = r;
//					}
//					
//				}
//			}
//		}
//		System.out.println("RESULT :");
//		for(Box b : result){
//			System.out.println(b.toString());
//		}
//		
//		return result;
	}
	
	public int getStackHeight(ArrayList<Box> stack){
		int result = 0;
		for(Box b : stack){
			result += b.h;
		}
		return result;
	}
	
	public static void main(String[] args) {
		Random r = new Random();
		ArrayList<Box> boxes = new ArrayList<Box>();
		
		for(int i = 0 ; i < 3 ; i++){
			int n = r.nextInt(10);
//			boxes.add(new Box(n, n, n));
			boxes.add(new Box(r.nextInt(10), r.nextInt(10), r.nextInt(10)));
//			boxes.add(new Box(i+1, i+1, i+1));
		}
		System.out.println("INPUT :");
		for(Box b : boxes){
			System.out.println(b.toString());
		}
		Question13 test = new Question13();
		System.out.println(">>   "+test.stackOfBox(boxes));
		
		
	}

}

class Box{
	int num;
	int w;
	int h;
	int d;
	public Box(int w, int h, int d){
		this.w = w;
		this.h = h;
		this.d = d;
	}
	
	public boolean biggerThan(Box box){
		return this.w > box.w && this.h > box.h && this.d > box.d;
	}
	
	
	
	@Override
	public String toString(){
		return String.format("(%2d,%2d,%2d)", w, h, d);
	}
	
	
}