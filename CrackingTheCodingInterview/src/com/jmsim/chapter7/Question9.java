package com.jmsim.chapter7;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Question9 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CircularArray<Integer> test = new CircularArray<Integer>();
		test.put(1);
		Iterator<Integer> itr = test.iterator();
		int idx = 0;
//		while(idx++ < 10){
//			if(itr.hasNext()){
//				System.out.println(itr.next());
//			}
//		}
//		
		idx = 0;
		for(Integer i : test){
			if(idx++ > 10){
				break;
			}
			System.out.println(i);
		}
	}

}
class CircularArray<T> implements Iterable<T>{
	
	List<T> list = new LinkedList<T>();
	
	public T get(int pos){
		return list.get((int)(pos % list.size()));
	}
	
	public void put(T val){
		list.add(val);
	}
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new CircularArrayIterator<T>(this);
	}
	
	class CircularArrayIterator<T2> implements Iterator<T2>{
		private int pos = -1;
		private List<T2> list = null;
		public CircularArrayIterator(CircularArray<T2> arr){
			list = arr.list;
		}
		@Override
		public boolean hasNext() {
			if(list.size() == 0){
				return false;
			}

			return true;
		}
		@Override
		public T2 next() {
			return list.get( (++pos)%list.size() );
		}
		
		
	}
	
}