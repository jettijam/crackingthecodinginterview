package com.jmsim.chapter7;

import java.util.ArrayList;
import java.util.Collections;

public class Question6 {
	JigsawPuzzle puzzle = new JigsawPuzzle(10);

	public void finishJigsawPuzzle(){
		ArrayList<Piece> done = new ArrayList<Piece>();
		for(int y = 1 ; y < 10+1 ; y++){
			for(int x = 1 ; x < 10+1 ; x++){
				if(puzzle.board[y][x] != null){
					continue;
				}

				for(Piece p : puzzle.pieces){
					if(done.contains(p))
						continue;
					if(p.fitsWith(puzzle.board[y-1][x], true) ||
							p.fitsWith(puzzle.board[y][x-1], false) ||
							p.fitsWith(puzzle.board[y+1][x], true) ||
							p.fitsWith(puzzle.board[y][x+1], false)){
						puzzle.board[y][x] = p;
						done.add(p);
						break;
					}
					//						if(puzzle.board[y-1][x].fitsWith(p) ||
					//								puzzle.board[y][x-1].fitsWith(p) ||
					//								puzzle.board[y+1][x].fitsWith(p) ||
					//								puzzle.board[y][x+1].fitsWith(p)){
					//							puzzle.board[y][x] = p;
					//							done.add(p);
					//							break;
					//						}

				}
			}
		}
		for(Piece p : done){
			puzzle.pieces.remove(p);
		}
		puzzle.printPuzzle();
		System.out.println();


	}

	public static void main(String[] args){
		Question6 test = new Question6();
		test.finishJigsawPuzzle();
	}
}
class JigsawPuzzle{
	Piece board[][];
	public ArrayList<Piece> pieces = new ArrayList<Piece>();

	public JigsawPuzzle(int N){
		board = new Piece[N+2][N+2];
		for(int y = 0 ; y < N+2 ; y++){
			for(int x = 0 ; x < N+2 ; x++){
				if(y == 0 || y == N+1){
					board[y][x] = new Edge(x, y);
				}else if(x == 0 || x == N+1){
					board[y][x] = new Edge(x, y);
				}else{
					pieces.add(new Piece(x, y));
				}
			}
		}
		Collections.shuffle(pieces);
	}

	public void printPuzzle(){
		for(int y = 0 ; y < board.length ; y++){
			for(int x = 0 ; x < board.length ; x++){
				if(board[y][x] == null){
					System.out.print(String.format("%10s", "X"));
				}else{
					System.out.print(String.format("%10s", board[y][x].name));
				}
			}
			System.out.println();
		}
	}
}
class Piece{
	public String name;
	private int x;
	private int y;
	public Piece(int x, int y){
		this.x = x;
		this.y = y;
		this.name = String.format("(%2d,%2d)", x,y);//"("+String.valueOf(x)+","+String.valueOf(y)+")";
	}
	public int getX(){
		return this.x;
	}
	public int getY(){
		return this.y;
	}
	public boolean fitsWith(Piece other, boolean isVertical){
		if(other == null) 
			return false;
		if(isVertical)
			return this.x == other.getX() && abs(this.y - other.getY()) == 1;
		return this.y == other.getY() && abs(this.x - other.getX()) == 1;
		//		if( abs(this.x - other.getX()) + abs(this.y - other.getY())  == 1){
		//			return true;
		//			
		//		}
		//		return false;

	}
	private int abs(int a){

		if(a >= 0)
			return a;
		return -a;
	}
}
class Edge extends Piece{

	public Edge(int x, int y) {
		super(x, y);
		this.name = "EDGE";
		// TODO Auto-generated constructor stub
	}

}