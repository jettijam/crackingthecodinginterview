package com.jmsim.chapter7;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

public class Question12 {
	public static void main(String[] args){
		HashTable table = new HashTable();
		table.put("AAA", "AAA_VAL");
		table.put("ABB", "ABB_VAL");
		
		System.out.println(table.get("AAA"));
		System.out.println(table.get("ABB"));
		table.put("ABB", "ABB_VAL2");
		System.out.println(table.get("ABB"));
	}
	
}


class HashTable{ // hash function f : String -> Integer
	Map<Integer, LinkedList<Pair<String, String>>> map = new HashMap<Integer, LinkedList<Pair<String, String>>>(); // Pair stores original String
	
	public String get(String input){
		Integer key = hashFunc(input);
		if(map.containsKey(key)){
			Iterator<Pair<String, String>> itr = map.get(key).iterator();
			while(itr.hasNext()){
				Pair<String, String> p = itr.next();
				if(p.first.equals(input)){
					return p.second;
				}
			}
			
		}
		return null;
	}
	
	public void put(String input, String value){
		Integer key = hashFunc(input);
		if(!map.containsKey(key)){
			map.put(key,  new LinkedList<Pair<String, String>>());	
		}
		
		Iterator<Pair<String, String>> itr = map.get(key).iterator();
		while(itr.hasNext()){
			Pair<String, String> p = itr.next();
			if(p.first.equals(input)){
				p.second = value;
				return;
			}
		}
		
		map.get(key).addFirst(new Pair<String, String>(input, value));
	}
	
	private Integer hashFunc(String input){
		return (int)input.charAt(0);
	}
	
}

class Pair<T, K>{
	T first;
	K second;
	public Pair(T f, K s){
		first = f;
		second = s;
	}
	
	public T first(){
		return first;
	}
	public K second(){
		return second;
	}
}

