package com.jmsim.chapter5;

public class Question2 {
	public String binaryToString(double num){
		StringBuilder resultBuilder = new StringBuilder();
		int cnt = 0;
		double firstVal = Double.NaN;
		while(true){
			double tmp = num*2;
			int intVal = (int) (tmp);
			double doubleVal = Double.valueOf("0."+String.valueOf(tmp).split("\\.")[1]);
//			System.out.println("At cnt :"+cnt+"\n intval "+intVal+"\n doubleVal "+doubleVal);
			resultBuilder.append(String.valueOf(intVal));
			
			if(cnt == 0){
				firstVal = doubleVal;
			}
			
			if(doubleVal == 0){
				break;
			}
			if(cnt > 0 && doubleVal == firstVal){
				break;
			}
			
			num = doubleVal;
			if(cnt++ > 32) return "ERROR";
			
		}
		return resultBuilder.toString();
		
	}
	
	public static void main(String[] args){
		Question2 test = new Question2();
		System.out.println(test.binaryToString(0.3));
	}
}
