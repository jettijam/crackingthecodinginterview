package com.jmsim.chapter5;

public class Question1 {
	public int insertion(int N, int M, int i, int j){
		StringBuilder maskString = new StringBuilder("");
		String binN = Integer.toBinaryString(N);
		for(int idx = 0 ; idx < binN.length() ; idx++){
			if(idx < binN.length() - j -1){
				maskString.append("1");
			}else if(idx >= binN.length()-i){
				maskString.append("1");
			}else{
				maskString.append("0");
			}
			
		}
		System.out.println(maskString);
		int mask = Integer.parseInt(maskString.toString(), 2);
		
		return (N&mask)|(M<<i);
	}
	
	public static void main(String[] args){
		Question1 test = new Question1();
		int result = test.insertion(Integer.parseInt("11111111111", 2), Integer.parseInt("10011", 2), 2, 6);
		
		System.out.println(Integer.toBinaryString(result));
	}
}
