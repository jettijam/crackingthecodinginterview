package com.jmsim.chapter5;

public class Question8 {
	public void drawLine(byte[] screen, int width, int x1, int x2, int y){
		int height = screen.length / (width/8); //width 16 means a row is 16 bits (2 byte).
		if(y >= height){
			System.out.println("Array index out of bounds!");
			return;
		}
		if(x1 > width || x2 > width){
			System.out.println("Array index out of bounds!");
			return;
		}
		
		int x1_byte = x1/8;
		int x2_byte = x2/8;
		for(int x = 0 ; x < (width/8) ; x++){
			if(x > x1_byte && x < x2_byte){
				screen[y*(width/8)+x] = (byte) 0xff;
				continue;
			}
			if(x==x1_byte){
				screen[y*(width/8)+x] = (byte) (0xff>>>(x1-((x1/8)*8)));
			}
			if(x== x2_byte){
				screen[y*(width/8)+x] = (byte) (0xff<<((((x2/8)+1)*8)-x2));

			}
			
			
		}
		
	}
	
	public void printScreen(byte[] screen, int width){
		int height = screen.length / (width/8);
		
		for(int y = 0 ; y < height ; y++){
			for(int x = 0 ; x < (width/8) ; x++){
				System.out.print(String.format("%8s", Integer.toBinaryString(screen[y*(width/8)+x] & 0xff)).replace(' ', '0'));
			}
			System.out.println();
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		byte[] screen = new byte[8];
		int width = 32;
		Question8 test = new Question8();
		test.printScreen(screen, width);
		System.out.println("##########################");
		test.drawLine(screen, width, 1,  30,  1);
		test.printScreen(screen, width);
	}

}
