package com.jmsim.chapter5;

public class Question6 {
	public int conversion(int num1, int num2){
		int len1 = (int)(Math.log(num1)/Math.log(2)) + 1;
		int len2 = (int)(Math.log(num2)/Math.log(2)) + 1;
		int len = Math.max(len1,  len2);
		int cnt = 0;
		int xor = num1^num2;
		for(int i = 0 ; i < len ; i++){
			if( (xor&1) == 1 )
				cnt ++;
			xor >>= 1;
		}
		
		return cnt;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Question6 test = new Question6();
		System.out.println(test.conversion(29, 15));
	}

}
