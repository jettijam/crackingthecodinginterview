package com.jmsim.chapter5;

import java.util.ArrayList;

public class Question3 {
	public int flipToWin(int num){
		String bit = Integer.toBinaryString(num);
		ArrayList<Integer> zeroIndices = new ArrayList<Integer>();
		for(int i = 0 ; i < bit.length() ; i++){
			if(bit.charAt(i) == '0'){
				zeroIndices.add(i);
			}
		}
		
		int result = Integer.MIN_VALUE;
		for(int zeroIdx : zeroIndices){
			String left = bit.substring(0, zeroIdx);
			String right = bit.substring(zeroIdx+1);
			String replaced = left+"1"+right;
			String[] split = replaced.split("0");
			
			for(String s : split){
				result = Math.max(result, s.length());
			}
		}
		
		return result;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Question3 test = new Question3();
		System.out.println(test.flipToWin(1775));
	}

}
